import { Frame, Atributo } from "../node_modules/@amazoniajs/core/core.js";

export class AmazoniaAssets extends Frame {
    constructor () {
        super({
            atributos: [
                AmIf,
                AmRepeat,
                AmChecked,
                AmSelected,
                AmSubmit,
                AmValue,
                AmClick,
                AmClass,
                AmModel,
            ]
        });
    }
}

class AmIf extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    $render() {
        let $ctrl = this;
        let captured = $ctrl.value;
        let funct = function () {
            return eval (captured);
        }
        let data = funct.call ($ctrl.$scope);
        if (!data) {
            if (this.nodeElement.element) {
                this.app.convertNodeToNone (this.nodeElement);
            }
        } else {
            this.nodeElement.evaluated = false;
            
        }
    }
    
}
AmIf.nombre = 'am-if';
AmIf.create = AmIf;
AmIf.conditional = true;

class AmRepeat extends Atributo {
    constructor () {
        super();
        this.repeated = [];
    }

    controller () {
        let $ctrl = this;
        this.$init = function () {
        }

        this.$render = function () {
            
            let $ctrl = this;
            if (!this.nodeElement.repeatData) {
                this.nodeElement.first = true;
                this.repeated = [this.nodeElement];
                this.cloned = this.app.cloneNodeElement (this.nodeElement, true);
                //console.log (this.cloned);
            }
            
            if (this.nodeElement.first) {
                //console.log (this.cloned);
                let exp = /^let ([a-z|A-Z_]+[a-zA-Z\d_]*) in ([a-z|A-Z_]+\w*[.?\w\[\]]*)$/;
                
                let captured = exp.exec ($ctrl.value);
                if (!captured || captured.length <= 0) {
                    throw "Error: The am-repeat attribute doesn't have a well formatted text";
                }
                
                let funct = function() {
                    return eval (captured[2]);
                };

                let data = funct.call(this.$scope);
                this.nodeElement.repeatData = data;
                
                if (this.nodeElement.repeatData.length > 0) {
                    this.nodeElement.repeatContent = this.nodeElement.repeatData[0];
                }

                let count = 0;
                for (let i in this.nodeElement.repeatData) {
                    if (!this.repeated[i]) {
                        
                        let clone = this.app.cloneNodeElement (this.cloned, true);
                        
                        //console.log (clone);
                        this.nodeElement.parent.append (clone);
                        //this.app.appendNodeElement (this.nodeElement.parent, clone);
                        this.repeated[i] = clone;
                    }

                    this.repeated[i].visible = true;
                    this.repeated[i].repeatRenderizable = captured[1];
                    this.repeated[i].repeatData = this.nodeElement.repeatData;
                    this.repeated[i].repeatContent = this.nodeElement.repeatData[i];
                    this.repeated[i].repeatIndex = i;

                    count++;
                    //console.log (count);
                }
                
                for (let i = count; i < this.repeated.length; i++) {
                    if (this.repeated[i].first) {
                        this.repeated[i].visible = false;
                    } else {
                        this.app.removeNodeElement (this.repeated[i]);
                    }
                }
                this.repeated.splice (Math.max (1, count), this.repeated.length);
                //console.log (this.repeated, this.nodeElement.repeatData, count);
            }
            //console.log (this.repeated);
            
            this.$scope[this.nodeElement.repeatRenderizable] = this.nodeElement.repeatContent;
            this.$scope.$index = this.nodeElement.repeatIndex;
        }
    }
}

AmRepeat.nombre = 'am-repeat';
AmRepeat.create = AmRepeat;
AmRepeat.conditional = true;

class AmChecked extends Atributo {
    constructor () {
        super();
    }

    $render () {
        let $ctrl = this;
        let captured = 'capturedText = ' + $ctrl.value;
        let funct = function () {
            let capturedText;
            eval (captured);
            return capturedText;
        };

        let data = funct.call(this.$scope);
        this.element.checked = !!data;
        
    }
}

AmChecked.nombre = "am-checked";
AmChecked.create = AmChecked;

class AmSelected extends Atributo {
    constructor () {
        super();
    }

    $render () {
        let $ctrl = this;
        let captured = 'capturedText = ' + $ctrl.value;
        let funct = function () {
            let capturedText;
            eval (captured);
            return capturedText;
        };

        let data = funct.call(this.$scope);

        
        if (data) {
            this.element.selected = true;
            
        }
            
        
    }
}

AmSelected.nombre = "am-selected";
AmSelected.create = AmSelected;

class AmClick extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    controller () {
        let $ctrl = this;
        $ctrl.$init = function () {

            $ctrl.element.addEventListener ('click', function () {
                $ctrl.hasToRun = true;
                setTimeout (() => {$ctrl.app.aplicar();$ctrl.app.aplicar();$ctrl.app.aplicar();}, 0);
            });
        }
        
    }

    $render() {
        if (this.hasToRun) {
            let $ctrl = this;
            let captured = $ctrl.value;
            let funct = function () {
                eval (captured);
            };
            funct.call($ctrl.$scope);
            this.hasToRun = false;
        }
        
    }
    
}

AmClick.nombre = "am-click";
AmClick.create = AmClick;


class AmValue extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    controller () {
        let $ctrl = this;
        $ctrl.$init = function () {
            $ctrl.app.captureWatch ($ctrl);
        }
        $ctrl.$render = function () {
            $ctrl.element.value = $ctrl.getValue();
        }
    }
    
}

AmValue.nombre = "am-value",
AmValue.create = AmValue;


class AmSubmit extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    controller () {
        let $ctrl = this;
        $ctrl.$init = function () {
            let $ctrl = this;
            $ctrl.element.addEventListener ('submit', function (evt) {
                evt.preventDefault();
                
                let captured = $ctrl.value;
                let funct = function () {
                    eval (captured);
                };
                funct.call($ctrl.$scope);
                
                setTimeout (() => {$ctrl.app.aplicar();$ctrl.app.aplicar();}, 0);
            });
        }
        
    }

    $render() {
        
    }
    
}

AmSubmit.nombre = "am-submit",
AmSubmit.create = AmSubmit;

class AmClass extends Atributo {
    constructor () {
        super();
    }

    controller () {
        let $ctrl = this;
        
        this.$render = function () {
            let captured = 'capturedText = ' + $ctrl.value;
            let funct = function () {
                
                let capturedText;
                eval (captured);
                console.log (capturedText);
                return capturedText;
            };

            let data = funct.call(this.$scope);
            for (let i in data) {
                if (data[i]) {
                    this.element.classList.add(i);
                } else {
                    this.element.classList.remove(i);
                }
            }
        }
    }
    
}

AmClass.nombre = "am-class";
AmClass.create = AmClass;


class AmModel extends Atributo {
    constructor () {
        super();
    }

    controller () {
        let $ctrl = this;
        let lastPrinted;

        $ctrl.$init = function () {
            $ctrl.app.captureWatch ($ctrl);
            if ($ctrl.element.nodeName == 'INPUT') {
                if ($ctrl.element.getAttribute ('type') == null
                    || $ctrl.element.getAttribute ('type') == 'text'
                    || $ctrl.element.getAttribute ('type') == 'password'
                    || $ctrl.element.getAttribute ('type') == 'hidden') {
                        
                    $ctrl.element.addEventListener ('input', function () {
                        $ctrl.asignValue ($ctrl.element.value);
                        setTimeout (() => {$ctrl.app.aplicar()}, 1);
                    });
                }

                if ($ctrl.element.getAttribute ('type') == 'checkbox') {
                    $ctrl.element.addEventListener ('change', function () {
                        $ctrl.hasToRun = true;
                        setTimeout (() => {$ctrl.app.aplicar();$ctrl.app.aplicar();}, 1);
                    });
                }

                if ($ctrl.element.getAttribute ('type') == 'radio') {
                    $ctrl.element.addEventListener ('change', function() {
                        $ctrl.hasToRun = true;
                        setTimeout (() => {$ctrl.app.aplicar();$ctrl.app.aplicar();}, 1);
                    })
                }
                
            } else if ($ctrl.element.nodeName == 'TEXTAREA') {
                $ctrl.element.addEventListener ('input', function () {
                    $ctrl.asignValue($ctrl.element.value);
                    setTimeout (() => {$ctrl.app.aplicar()});
                });
            } else if ($ctrl.element.nodeName == 'SELECT') {
                $ctrl.element.addEventListener ('change', function () {
                    let value = $ctrl.element.multiple ? Array.from($ctrl.element.selectedOptions).map(v=>v.value) : $ctrl.element.value;
                    $ctrl.asignValue(value);
                    setTimeout (() => {$ctrl.app.aplicar()});
                })
            }
            
        }

        $ctrl.$render = function () {
            let curr = $ctrl.getValue();
            let haveToChange = lastPrinted !== curr;
            lastPrinted = curr;
            
            
            switch ($ctrl.element.nodeName) {
                case 'INPUT':
                    if ($ctrl.element.getAttribute ('type') == null
                        || $ctrl.element.getAttribute ('type') == 'text'
                        || $ctrl.element.getAttribute ('type') == 'password'
                        || $ctrl.element.getAttribute ('type') == 'hidden') {
                        if ($ctrl.element.value !== curr) {
                            $ctrl.element.value = curr;
                        }
                        
                    }
                        
                    if ($ctrl.element.getAttribute ('type') == 'checkbox') {
                        if ($ctrl.hasToRun) {
                            $ctrl.asignValue($ctrl.element.checked);
                            $ctrl.hasToRun = false;
                        }
                        if ($ctrl.element.checked != curr) {
                            $ctrl.element.checked = curr;
                        }
                    }

                    if ($ctrl.element.getAttribute ('type') == 'radio') {
                        if ($ctrl.hasToRun) {
                            $ctrl.asignValue ($ctrl.element.value);
                            $ctrl.hasToRun = false;
                        }
                        if ($ctrl.element.checked != ($ctrl.element.value == curr)) {
                            $ctrl.element.checked = $ctrl.element.value == curr;
                        }
                        
                    }
                    break;
                case 'TEXTAREA':
                    if ($ctrl.element.value !== curr) {
                        $ctrl.element.value = curr;
                    }
                        
                    
                    break;
                case 'SELECT':
                    if ($ctrl.element.multiple) {
                        let vals = $ctrl.getValue();
                        for (let i = 0; i < $ctrl.element.options.length; i++) {
                            let e = $ctrl.element.options[i];
                            if (e.selected != vals.indexOf (e.value) >= 0) {
                                e.selected = vals.indexOf (e.value) >= 0;
                            }
                        }
                    } else {
                        if ($ctrl.element.value != curr) {
                            $ctrl.element.value = curr;
                        }
                    }
                    break;
            }
        }
    }
    
}

AmModel.nombre = "am-model";
AmModel.create = AmModel;
