import { Etiqueta } from "../node_modules/@amazoniajs/core/core.js";

export class AppTag extends Etiqueta {

    list = [];
    model;

    constructor (app) {
        super (app);
    }

    $init() {
        this.list = [];
        this.model = '';
    }

    add () {
        if (this.model) {
            this.list.push (this.model);
        }
        this.model = '';
    }

    remove (index) {
        this.list.splice (index, 1);
        console.log (this.list);
    }
}
AppTag.create = AppTag;
AppTag.ruta = 'app/app.tag.html';
AppTag.nombre = 'app-tag';