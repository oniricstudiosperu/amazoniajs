import { Frame } from "../node_modules/@amazoniajs/core/core.js";
import { AmazoniaAssets } from "../amazonia-assets/amazonia-assets.frame.js";
import { AppTag } from "./app.tag.js";

export class App extends Frame {
    constructor () {
        super({
            componentes: [AppTag],
            modules: [AmazoniaAssets]
        });
    }
};