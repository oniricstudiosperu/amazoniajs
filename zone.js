const fs = require('fs');
var myArgs = process.argv.slice(2);

let tag = `import { Etiqueta } from "<from>media/amazoniaJS/amazonia.js";

export class <tagclass> extends Etiqueta {
    constructor (app) {
        super (app);
    }
}
<tagclass>.create = <tagclass>;
<tagclass>.ruta = '<htmlroute>';
<tagclass>.nombre = '<tagname>';`;
let html = `<div>Funciona!</div>`;

class Proccess {
    static generate (myArgs) {
        switch (myArgs[1]) {
            case 'tag':
            case 't':
                this.generate_tag(myArgs);
                break;
        }
    }

    static existsFile (file) {
        try {
            fs.readFileSync (file);
            console.log ('File ' + file + ' already exists');
            return true;
        } catch (e) { }
        return false;
    }

    static createFile (inspect, generate) {
        let pathDir = inspect.path;
        let fileName = inspect.last;

        let path = 'app/' + pathDir + '/';
        let dir = __dirname + '/' + path;

        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }

        generate (path, dir, fileName);
        
    }
    static generate_tag(myArgs) {
        let bottom = 2;
        let atleast = false;
        // all args from bottom are files with name
        for (let i = bottom; i < myArgs.length; i++) {
            atleast = true;
            let inspect = this.inspectPath(myArgs[i]);
            this.createFile(inspect, (path, dir, fileName) => {
                let fileJS = dir + fileName + '.tag.js';
                let templatePath = path + fileName + '.tag.html';
                let fileHTML = dir + fileName + '.tag.html';
                
                if (this.existsFile (fileJS) || this.existsFile(fileHTML)) {
                    return;
                }
                
                let from = '';
                for (let i = 0; i <= inspect.dirLength; i++) {
                    from += '../'
                }
                let tagsplit = fileName.split('-');
                let tagClass = '';
                for (let i in tagsplit) {
                    tagClass += tagsplit[i][0].toUpperCase() + tagsplit[i].slice(1);
                }
                tagClass += 'Tag';
                
                let changed = tag.replace('<tagname>', 'app-' + fileName.toLowerCase())
                    .replace('<from>', from)
                    .replace('<htmlroute>', templatePath)
                    .replace (/<tagclass>/g, tagClass);
                
                fs.writeFileSync (fileJS, changed);
                fs.writeFileSync (fileHTML, html);
            });
        }
        if (!atleast) {
            console.log ('generate tag must have at least one tag to generate');
        }
        let creation = myArgs[2];
    }
    /**
     * 
     */
    static inspectPath (path) {
        
        let portions = path.split('/');
        if (path[path.length - 1] == '/') {
            path.substring(0, path.length - 2);
        }
        return {
            path: path,
            last: portions[portions.length - 1],
            dirLength: portions.length
        };
    }
}

switch (myArgs[0]) {
    case 'generate':
    case 'g':
        Proccess.generate(myArgs);
        break;
    case 'compliment':
        console.log(myArgs[1], 'is really cool.');
        break;
    default:
        console.log('Sorry, that is not something I know how to do.');
}

