import { Frame, Atributo } from "../media/amazoniaJS/amazonia.js";

export class AmazoniaAssets extends Frame {
    constructor () {
        super({
            atributos: [AmIf, AmModel, AmRepeat, AmSubmit, AmValue, AmClick]
        });
    }
}

class AmIf extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    $render() {
        let $ctrl = this;
        let captured = $ctrl.value;
        let funct = function () {
            return eval (captured);
        }
        let data = funct.call ($ctrl.$scope);
        if (!data) {
            if (this.nodeElement.element) {
                this.app.convertNodeToNone (this.nodeElement);
            }
        } else {
            this.nodeElement.evaluated = false;
            
        }
    }
    
}
AmIf.nombre = 'am-if';
AmIf.create = AmIf;

class AmRepeat extends Atributo {
    constructor () {
        super();
        this.repeated = [];
    }

    controller () {
        let $ctrl = this;
        this.$init = function () {
        }

        this.$render = function () {
            
            let $ctrl = this;
            if (!this.nodeElement.repeatData) {
                console.log (this.nodeElement.repeatData);
                this.nodeElement.first = true;
                this.repeated = [this.nodeElement];
            }
            
            if (this.nodeElement.first) {
                console.log (this.nodeElement, this.repeated);
                this.cloned = this.app.cloneNodeElement (this.nodeElement, true);
                let exp = /^let ([a-z|A-Z_]+[a-zA-Z\d_]*) in ([a-z|A-Z_]+\w*[.?\w\[\]]*)$/;
            
                let captured = exp.exec ($ctrl.value);
                if (!captured || captured.length <= 0) {
                    throw "Error: The am-repeat attribute doesn't have a well formatted text";
                }
                
                let funct = function () {
                    return eval (captured[2]);
                };

                let data = funct.call(this.$scope);
                
                this.nodeElement.repeatData = data;
                
                if (this.nodeElement.repeatData.length > 0) {
                    this.nodeElement.repeatContent = this.nodeElement.repeatData[0];
                }
                
                let count = 0;
                for (let i in this.nodeElement.repeatData) {
                    if (!this.repeated[i]) {
                        //console.log (i, this.repeated);
                        //console.log ('attached');
                        let clone = this.app.cloneNodeElement (this.cloned, true);
                        this.app.appendNodeElement (this.nodeElement.parent, clone);
                        this.repeated[i] = clone;
                    }

                    this.repeated[i].evaluated = false;
                    this.repeated[i].repeatRenderizable = captured[1];
                    this.repeated[i].repeatData = this.nodeElement.repeatData;
                    this.repeated[i].repeatContent = this.nodeElement.repeatData[i];
                    this.repeated[i].repeatIndex = i;

                    count++;
                    //console.log (count);
                }
                
                //console.log (this.repeated, this.nodeElement.repeatData, count);
                for (let i = count; i < this.repeated.length; i++) {
                    if (this.repeated[i].first) {
                        this.app.convertNodeToNone (this.repeated[i]);
                    } else {
                        this.app.removeNodeElement (this.repeated[i].parent, this.repeated[i]);
                        if (this.repeated[i].element.parentNode) {
                            this.repeated[i].element.parentNode.removeChild (this.repeated[i].element);
                        }
                        
                    }
                }
                //console.log (this.repeated, this.nodeElement.repeatData, count);
            }
            
            this.$scope[this.nodeElement.repeatRenderizable] = this.nodeElement.repeatContent;
            this.$scope.$index = this.nodeElement.repeatIndex;
        }
    }
}

AmRepeat.nombre = 'am-repeat';
AmRepeat.create = AmRepeat;
/*{
class Attribute extends amazonia.atributo {
    constructor () {
        super();
    }

    $render () {
        let $ctrl = this;
        let captured = 'capturedText = ' + $ctrl.value;
        let funct = function () {
            let capturedText;
            eval (captured);
            return capturedText;
        };

        let data = funct.call(this.$scope);

        this.element.checked = !!data;
        
        
    }
}

amazonia.apps["super"].atributos.push({
    nombre: "am-checked",
    create: Attribute
});
}
{
class Attribute extends amazonia.atributo {
    constructor () {
        super();
    }

    $render () {
        let $ctrl = this;
        let captured = 'capturedText = ' + $ctrl.value;
        let funct = function () {
            let capturedText;
            eval (captured);
            return capturedText;
        };

        let data = funct.call(this.$scope);

        
        if (data) {
            this.element.selected = true;
            
        }
            
        
    }
}

amazonia.apps["super"].atributos.push({
    nombre: "am-selected",
    create: Attribute
});
}
*/

class AmClick extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    controller () {
        let $ctrl = this;
        $ctrl.$init = function () {

            $ctrl.element.addEventListener ('click', function () {
                $ctrl.hasToRun = true;
                setTimeout (() => {$ctrl.app.aplicar();$ctrl.app.aplicar();$ctrl.app.aplicar();}, 0);
                
            });
        }
        
    }

    $render() {
        if (this.hasToRun) {
            let $ctrl = this;
            let captured = $ctrl.value;
            let funct = function () {
                eval (captured);
            };
            funct.call($ctrl.$scope);
            this.hasToRun = false;
        }
        
    }
    
}

AmClick.nombre = "am-click";
AmClick.create = AmClick;


class AmValue extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    controller () {
        let $ctrl = this;
        $ctrl.$init = function () {
            $ctrl.app.captureWatch ($ctrl);
        }
        $ctrl.$render = function () {
            $ctrl.element.value = $ctrl.getValue();
        }
    }
    
}

AmValue.nombre = "am-value",
AmValue.create = AmValue;


class AmSubmit extends Atributo {
    constructor () {
        super();
        this.hasToRun = false;
    }

    controller () {
        let $ctrl = this;
        $ctrl.$init = function () {
            let $ctrl = this;
            $ctrl.element.addEventListener ('submit', function (evt) {
                evt.preventDefault();
                
                let captured = $ctrl.value;
                let funct = function () {
                    eval (captured);
                };
                funct.call($ctrl.$scope);
                
                setTimeout (() => {$ctrl.app.aplicar();}, 0);
            });
        }
        
    }

    $render() {
        
    }
    
}

AmSubmit.nombre = "am-submit",
AmSubmit.create = AmSubmit;

/*

{
class Attribute extends amazonia.atributo {
    constructor () {
        super();
    }

    controller () {
        let $ctrl = this;
        
        this.$render = function () {
            let captured = 'capturedText = ' + $ctrl.value;
            let funct = function () {
                let capturedText;
                eval (captured);
                return capturedText;
            };

            let data = funct.call(this.$scope);
            for (let i in data) {
                if (data[i]) {
                    this.element.classList.add(i);
                } else {
                    this.element.classList.remove(i);
                }
            }
        }
    }
    
}

amazonia.apps["super"].atributos.push({
    nombre: "am-class",
    create: Attribute
});
}
*/

class AmModel extends Atributo {
    constructor () {
        super();
    }

    controller () {
        let $ctrl = this;
        let lastPrinted;

        $ctrl.$init = function () {
            $ctrl.app.captureWatch ($ctrl);
            if ($ctrl.element.nodeName == 'INPUT') {
                if ($ctrl.element.getAttribute ('type') == null
                    || $ctrl.element.getAttribute ('type') == 'text'
                    || $ctrl.element.getAttribute ('type') == 'password'
                    || $ctrl.element.getAttribute ('type') == 'hidden') {
                        
                    $ctrl.element.addEventListener ('input', function () {
                        $ctrl.asignValue ($ctrl.element.value);
                        setTimeout (() => {$ctrl.app.aplicar()}, 1);
                    });
                }

                if ($ctrl.element.getAttribute ('type') == 'checkbox') {
                    $ctrl.element.addEventListener ('change', function () {
                        $ctrl.hasToRun = true;
                        setTimeout (() => {$ctrl.app.aplicar();$ctrl.app.aplicar();}, 1);
                    });
                }

                if ($ctrl.element.getAttribute ('type') == 'radio') {
                    $ctrl.element.addEventListener ('change', function() {
                        $ctrl.hasToRun = true;
                        setTimeout (() => {$ctrl.app.aplicar();$ctrl.app.aplicar();}, 1);
                    })
                }
                
            } else if ($ctrl.element.nodeName == 'TEXTAREA') {
                $ctrl.element.addEventListener ('input', function () {
                    $ctrl.asignValue($ctrl.element.value);
                    setTimeout (() => {$ctrl.app.aplicar()});
                });
            } else if ($ctrl.element.nodeName == 'SELECT') {
                $ctrl.element.addEventListener ('change', function () {
                    let value = $ctrl.element.multiple ? Array.from($ctrl.element.selectedOptions).map(v=>v.value) : $ctrl.element.value;
                    $ctrl.asignValue(value);
                    setTimeout (() => {$ctrl.app.aplicar()});
                })
            }
            
        }

        $ctrl.$render = function () {
            let curr = $ctrl.getValue();
            let haveToChange = lastPrinted !== curr;
            lastPrinted = curr;
            
            
            switch ($ctrl.element.nodeName) {
                case 'INPUT':
                    if ($ctrl.element.getAttribute ('type') == null
                        || $ctrl.element.getAttribute ('type') == 'text'
                        || $ctrl.element.getAttribute ('type') == 'password'
                        || $ctrl.element.getAttribute ('type') == 'hidden') {
                        if ($ctrl.element.value !== curr) {
                            $ctrl.element.value = curr;
                        }
                        
                    }
                        
                    if ($ctrl.element.getAttribute ('type') == 'checkbox') {
                        if ($ctrl.hasToRun) {
                            $ctrl.asignValue($ctrl.element.checked);
                            $ctrl.hasToRun = false;
                        }
                        if ($ctrl.element.checked != curr) {
                            $ctrl.element.checked = curr;
                        }
                    }

                    if ($ctrl.element.getAttribute ('type') == 'radio') {
                        if ($ctrl.hasToRun) {
                            $ctrl.asignValue ($ctrl.element.value);
                            $ctrl.hasToRun = false;
                        }
                        if ($ctrl.element.checked != ($ctrl.element.value == curr)) {
                            $ctrl.element.checked = $ctrl.element.value == curr;
                        }
                        
                    }
                    break;
                case 'TEXTAREA':
                    if ($ctrl.element.value !== curr) {
                        $ctrl.element.value = curr;
                    }
                        
                    
                    break;
                case 'SELECT':
                    if ($ctrl.element.multiple) {
                        let vals = $ctrl.getValue();
                        for (let i = 0; i < $ctrl.element.options.length; i++) {
                            let e = $ctrl.element.options[i];
                            if (e.selected != vals.indexOf (e.value) >= 0) {
                                e.selected = vals.indexOf (e.value) >= 0;
                            }
                        }
                    } else {
                        if ($ctrl.element.value != curr) {
                            $ctrl.element.value = curr;
                        }
                    }
                    break;
            }
        }
    }
    
}

AmModel.nombre = "am-model";
AmModel.create = AmModel;
